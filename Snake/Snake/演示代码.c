#define _CRT_SECURE_NO_WARNINGS 1

#include <stdio.h>
#include <stdlib.h>

//system 函数可以用来执行系统命令

//int main()
//{
//	//设置控制台的相关属性
//	system("mode con cols=100 lines=30");
//	system("title 贪吃蛇");
//
//	//getchar();
//	system("pause");
//
//	return 0;
//}
#include <windows.h>
//
//
//int main()
//{
//	COORD pos1 = {0, 0};
//	COORD pos2 = { 10, 20 };
//
//
//	return 0;
//}
//

#include <stdbool.h>

//
//int main()
//{
//	//获得标准输出设备的句柄
//	HANDLE houtput = NULL;
//	houtput = GetStdHandle(STD_OUTPUT_HANDLE);
//
//	//定义一个光标信息的结构体
//	CONSOLE_CURSOR_INFO cursor_info = {0};
//
//	//获取和houtput句柄相关的控制台上的光标信息，存放在cursor_info中
//	GetConsoleCursorInfo(houtput, &cursor_info);
//
//	//修改光标的占比
//	//cursor_info.dwSize = 100;
//
//	cursor_info.bVisible = false;
//
//	//设置和houtput句柄相关的控制台上的光标信息
//	SetConsoleCursorInfo(houtput, &cursor_info);
//	
//	system("pause");
//	return 0;
//}
//





//int main()
//{
//	//获得标准输出设备的句柄
//	HANDLE houtput = NULL;
//	houtput = GetStdHandle(STD_OUTPUT_HANDLE);
//
//	//定位光标的位置
//	COORD pos = { 10, 20 };
//	SetConsoleCursorPosition(houtput, pos);
//
//	printf("hehe\n");
//	//getchar();
//	//system("pause");
//	return 0;
//}

//void set_pos(short x, short y)
//{
//	//获得标准输出设备的句柄
//	HANDLE houtput = NULL;
//	houtput = GetStdHandle(STD_OUTPUT_HANDLE);
//
//	//定位光标的位置
//	COORD pos = { x, y };
//	SetConsoleCursorPosition(houtput, pos);
//}
//
//int main()
//{
//	
//	set_pos(10, 20);
//	printf("hehe\n");
//
//	set_pos(10, 10);
//	printf("hehe\n");
//	getchar();
//	//system("pause");
//	return 0;
//}

//结果是1表示按过
//结果是0表示未按过
//
//#define KEY_PRESS(vk)  ((GetAsyncKeyState(vk)&1)?1:0)
//
//int main()
//{
//	short ret = GetAsyncKeyState(0x35);
//
//	if ((ret & 1) == 1)
//		printf("5被按过\n");
//	else
//		printf("没有被按过\n");
//	return 0;
//}
//

//
//#include <stdio.h>
//#include <windows.h>
//
//#define KEY_PRESS(vk)  ((GetAsyncKeyState(vk)&1)?1:0)
//
//int main()
//{
//    while (1)
//    {
//        if (KEY_PRESS(0x30))
//        {
//            printf("0\n");
//        }
//        else if (KEY_PRESS(0x31))
//        {
//            printf("1\n");
//        }
//        else if (KEY_PRESS(0x32))
//        {
//            printf("2\n");
//        }
//        else if (KEY_PRESS(0x33))
//        {
//            printf("3\n");
//        }
//        else if (KEY_PRESS(0x34))
//        {
//            printf("4\n");
//        }
//        else if (KEY_PRESS(0x35))
//        {
//            printf("5\n");
//        }
//        else if (KEY_PRESS(0x36))
//        {
//            printf("6\n");
//        }
//        else if (KEY_PRESS(0x37))
//        {
//            printf("7\n");
//        }
//        else if (KEY_PRESS(0x38))
//        {
//            printf("8\n");
//        }
//        else if (KEY_PRESS(0x39))
//        {
//            printf("9\n");
//        }
//    }
//    return 0;
//}

//
//#include <locale.h>
//
//int main()
//{
//	char* ret = setlocale(LC_ALL, NULL);
//	printf("%s\n", ret);
//
//	ret = setlocale(LC_ALL, "");//
//	printf("%s\n", ret);
//
//	return 0;
//}

#include <stdio.h>      /* printf */
#include <time.h>       /* time_t, struct tm, time, localtime, strftime */
#include <locale.h>     /* struct lconv, setlocale, localeconv */
//
//int main()
//{
//    time_t rawtime;
//    struct tm* timeinfo;
//    char buffer[80];
//
//    struct lconv* lc;
//
//    time(&rawtime);
//    timeinfo = localtime(&rawtime);
//
//    int twice = 0;
//
//    do {
//        printf("Locale is: %s\n", setlocale(LC_ALL, NULL));
//
//        strftime(buffer, 80, "%c", timeinfo);
//        printf("Date is: %s\n", buffer);
//
//        lc = localeconv();
//        printf("Currency symbol is: %s\n-\n", lc->currency_symbol);
//
//        setlocale(LC_ALL, "");
//    } while (!twice++);
//
//    return 0;
//}
//

//
//int main()
//{
//	//设置本地化
//	setlocale(LC_ALL, "");
//
//	char a = 'a';
//	char b = 'b';
//	printf("%c%c\n", a, b);
//
//	wchar_t wc1 = L'比';
//	wchar_t wc2 = L'特';
//	wprintf(L"%lc\n", wc1);
//	wprintf(L"%lc\n", wc2);
//	wprintf(L"%lc\n", L'●');
//	wprintf(L"%lc\n", L'★');
//
//	return 0;
//}


//int main()
//{
//	system("mode con cols=30 lines=30");
//
//	return 0;
//}

